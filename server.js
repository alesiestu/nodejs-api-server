const express = require('express');   //è il mio express per le chiamate rest API
const bodyParser = require('body-parser'); // lo uso per parsare le richieste


// create express app
const app = express();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

// Configuring the database
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Connessione corretta al database");    
}).catch(err => {
    console.log('Connessione NON corretta al database. tra poco errore...', err);
    process.exit();
});

// define a simple route
app.get('/', (req, res) => {
    res.json({"message": "Benvenuto nella banca dati"});



});



// Require Notes routes
require('./app/routes/product.routes.js')(app);
require('./app/routes/user.routes.js')(app);

const port = process.env.PORT || 3000;

// listen for requests
app.listen(port, () => {
    console.log("Il server è in ascolto sulla porta 3002");
});

