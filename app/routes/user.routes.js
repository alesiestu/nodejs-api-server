module.exports = (app) => {
    const users = require('../controllers/gestuser.controller.js');
    const cleanBody = require("../middlewares/cleanbody");
    const { validateToken } = require("../middlewares/validateToken");

  



    // Create a new Note
    app.post('/user', users.create);

    // Retrieve all Notes
    app.get('/users', users.findAll);

    // Retrieve a single Note with noteId
    app.get('/users/:prodId', users.findOne);

    // Update a Note with noteId
    app.put('/users/:prodId', users.update);

    // Delete a Note with noteId
    app.delete('/users/:prodId', users.delete);

    app.post('/user/login',cleanBody,users.Login);

    app.patch('/user/activate',users.Activate);


    app.patch("/forgot", cleanBody, users.ForgotPassword);

    app.patch("/reset", cleanBody, users.ResetPassword);

    app.get("/referred", validateToken, users.ReferredAccounts);

    app.get("/logout", validateToken, users.Logout);

}