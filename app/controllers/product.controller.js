const Product = require('../models/product.model.js');


// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
  
    if(!req.body.content) {
        return res.status(400).send({
            message: "Campo vuoto"
        });
    }

    // Create a Product
    const product = new Product({
        title: req.body.title || "Untitled Note", 
        content: req.body.content,
        name: req.body.name,
        stock:req.body.stock,
        reviews:req.body.reviews,
        stars:req.body.stars,
        price: req.body.price,
        image: req.body.image,
        images: req.body.images,
        colors: req.body.colors,
        company: req.body.company,
        description: req.body.description,
        category: req.body.category,
        shipping: req.body.shipping
    });

    // Save Note in the database
    product.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};




// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    var total=Product.length
    Product.find()
    .then(products => {
        res.set('Access-Control-Expose-Headers', 'X-Total-Count')
        res.set('X-Total-Count', total)
        let returnedProducts = []; 
		
		for (let i = 0; i < products.length; i++) {
			returnedProducts.push(products[i].transform());
		}

        res.send(returnedProducts);


    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

// Find a single note with a noteId
exports.findOne = (req, res) => {
    Product.findById(req.params.prodId)
    .then(product => {
        if(!product) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.prodId
            });            
        }
        res.send(product.transform());
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.prodId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.prodId
        });
    });
};



// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    // Find note and update it with the request body
    Product.findByIdAndUpdate(req.params.prodId, {
        
        
        title: req.body.title || "Untitled Note", 
        content: req.body.content,
        name: req.body.name,
        price: req.body.price,
        image: req.body.image,
        images: req.body.images,
        colors: req.body.colors,
        company: req.body.company,
        description: req.body.description,
        category: req.body.category,
        shipping: req.body.shipping



    }, {new: true})
    .then(product => {
        if(!product) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.prodId
            });
        }
        res.send(product.transform()); 
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.prodId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.prodId
        });
    });
};



// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Product.findByIdAndRemove(req.params.prodId)
    .then(product => {
        if(!product) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.prodId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.prodId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.prodId
        });
    });
};