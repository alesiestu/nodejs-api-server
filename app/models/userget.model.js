const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

var autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection("mongodb+srv://dbuser:divanetto90@iannareact.n7cmv.mongodb.net/myFirstDatabase?retryWrites=true&w=majority");

autoIncrement.initialize(connection);


const userSchema = mongoose.Schema({
    
    userId: { type: String, unique: true, required: true },
    email: { type: String, required: true, unique: true },
    active: { type: Boolean, default: false },
    password: { type: String, required: true },
    resetPasswordToken: { type: String, default: null },
    resetPasswordExpires: { type: Date, default: null },

    emailToken: { type: String, default: null },
    emailTokenExpires: { type: Date, default: null },

    accessToken: { type: String, default: null },

    referralCode: { type: String, unique: true },
    referrer: { type: String, default: null },  
 
    
}, {
    timestamps: true
});


userSchema.method('transform', function() {
    var obj = this.toObject();

    //  Rename fields
      obj.id = obj._id;
      delete obj._id;

     return obj;
});


userSchema.plugin(autoIncrement.plugin, 'user');
var user = connection.model('user', userSchema);



module.exports = mongoose.model('user', userSchema);

module.exports.hashPassword = async (password) => {
    try {
        const salt = await bcrypt.genSalt(10); // 10 rounds
        return await bcrypt.hash(password, salt);
    } catch (error) {
        throw new Error("Hashing failed", error);
    }
};

module.exports.comparePasswords = async (inputPassword, hashedPassword) => {
    try {
      return await bcrypt.compare(inputPassword, hashedPassword);
    } catch (error) {
      throw new Error("Comparison failed", error);
    }
  };

