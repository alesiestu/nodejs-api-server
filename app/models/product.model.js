const mongoose = require('mongoose');

var autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection("mongodb+srv://dbuser:divanetto90@iannareact.n7cmv.mongodb.net/myFirstDatabase?retryWrites=true&w=majority");

autoIncrement.initialize(connection);


const ProductSchema = mongoose.Schema({
    
    name: String,
    stock:Number,
    reviews:Number,
    stars:Number,
    price: Number,
    image: String,
    images: { type : Array , "default" : ['url','src'] },
    colors: { type : Array , "default" : [] },
    company: String,
    description: String,
    category: String,
    shipping: Boolean
}, {
    timestamps: true
});


ProductSchema.method('transform', function() {
    var obj = this.toObject();

    //  Rename fields
      obj.id = obj._id;
      delete obj._id;

     return obj;
});


ProductSchema.plugin(autoIncrement.plugin, 'Product');
var Product = connection.model('Product', ProductSchema);

module.exports = mongoose.model('Product', ProductSchema);